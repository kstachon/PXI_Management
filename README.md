This is a working PXI management app. 
The aim of the application is to collect data about calibration due of NI cards, especially DMMs, that are distributed among many chasis. 
The repository contains installer of a client app, that should be installed on all PXI systems and is logging relevant data to ELQA database. 

Detailed documentation is present in 'ELQA_PXI_Management_Client_docs' folder in the repository. 

Author: Krzysztof Stachon, 
E-mail: krzysztof.stachon@cern.ch