import decryptor as db          # for Oracle connection
import cx_Oracle                # for Oracle connection
import zlib                     # for MIC_DVC_SAMPLE_POINTS

conn    = cx_Oracle.connect(db.user, db.pswd, db.name)
version = conn.version
curr    = conn.cursor()

#list of exception keywords which cannot be used in database table and column names
keywords_list = [ # WARNING: Use lowercase!!!
    "select", "from", "where", "limit", "index"
    ]
timestamp_list = [ # WARNING: Use uppercase
    "EXT_CAL", "EXTERNAL_CAL_DUE", "SELF_CAL", "TIMESTAMP"
    ]

def getDbVersion():
    return version

def openConnection():
    if( conn == None):
        conn.cx_Oracle.connect(db.user, db.pswd, db.name)
        curr    = conn.cursor()  

def closeConnection():
    if( curr != None):
        curr.close()
    if( conn != None):
        conn.close()
    
def prepareSelectQuery(tableName, columnsNames, condition = ""):
    nbOfCol = len(columnsNames)
    query = "SELECT "
    i = 0
    for row in columnsNames:
        if( row.upper() in timestamp_list ):
            row = "TO_CHAR(" + row + ", 'YYYY-MM-DD HH24:MI:SS')"
        query += row
        i += 1
        if i < nbOfCol:
            query += ","      
    query += " FROM " + tableName 
    if( nbOfCol == 0 ):
        query = "SELECT * FROM " + tableName
    if(condition):
        query += " " + condition
    return query

def getOneDataValueFromDB(columnName, sourceTable, condition):
    query = "SELECT "+ columnName + " FROM " + sourceTable
    if(condition):
        query += " " +condition
    curr.execute(query)
    data = curr.fetchall()
    return str(data[0][0]) if (data) else ""
    
def getDataFromDB_asString( columnsNames, sourceTable, condition ):
    nbOfColumns = len(columnsNames)
    query = prepareSelectQuery(sourceTable, columnsNames, condition)
    curr.execute(query)
    if (nbOfColumns==0):
        nbOfColumns = len(curr.description)
    data = curr.fetchall()
    
    columns = [ [ (str(dat[i]) if (dat[i] != None) else "")  for i in range(nbOfColumns)] for dat in data ]
    return columns

def getDataFromDB_asInteger( columnsNames, sourceTable, condition ):
    nbOfColumns = len(columnsNames)
    query = prepareSelectQuery(sourceTable, columnsNames, condition)
    curr.execute(query)
    if (nbOfColumns==0):
        nbOfColumns = len(curr.description)
    data = curr.fetchall()
    
    columns = [ [ (int(dat[i]) if (dat[i] != None) else 0)  for i in range(nbOfColumns)] for dat in data ]
    return columns
    
def getDataFromDB_asFloat( columnsNames, sourceTable, condition ):
    nbOfColumns = len(columnsNames)
    query = prepareSelectQuery(sourceTable, columnsNames, condition)
    curr.execute(query)
    if (nbOfColumns==0):
        nbOfColumns = len(curr.description)
    data = curr.fetchall()
    
    columns = [ [ (float(dat[i]) if (dat[i] != None) else 0.0)  for i in range(nbOfColumns)] for dat in data ]
    return columns
    
    
def getData_asString( query ):
    curr.execute(query)
    nbOfColumns = len(curr.description)
    data = curr.fetchall()
    
    columns = [ [ (str(dat[i]) if (dat[i] != None) else "")  for i in range(nbOfColumns)] for dat in data ]
    return columns 
 
def getTableDescription(tableName, nbOfColumns=0):
    query = "SELECT * FROM " + tableName
    curr.execute(query)
    if(nbOfColumns==0):
        nbOfColumns = len(curr.description[0])
    columns = [ [ (str(dat[i]) if (dat[i] != None) else "")  for i in range(nbOfColumns)] for dat in curr.description ]
    return columns
   
def getOneColumnFromDB( query ):
    column_data = []
    curr.execute(query)
    data = curr.fetchall()
    for dat in data:
        column_data.append(str(dat[0]))  
    return column_data

# def OutputTypeHandler(cursor, name, defaultType, size, precision, scale):
    # if defaultType == cx_Oracle.DB_TYPE_CLOB:
        # return cursor.var(cx_Oracle.DB_TYPE_LONG, arraysize=cursor.arraysize)
    # if defaultType == cx_Oracle.DB_TYPE_BLOB:
        # return cursor.var(cx_Oracle.DB_TYPE_LONG_RAW, arraysize=cursor.arraysize)
        
# def getDataAsSpreedstring( query ):
    # conn.outputtypehandler = OutputTypeHandler
    # curr.execute(query)
    # data = curr.fetchone()[0]
    # #tmpData = bytes(data, 'utf-8')
    # #decompressed = zlib.decompress(tmpData)
    # #tmpData = bytes(data.read(), 'utf-8')
    # #
    # #decompressed = zlib.decompress(tmpData)
    # #tmpStr = tmpData.encode(encoding="ascii")
    # #decompressed = zlib.decompress(tmpStr)
    
    # return data
 
# def stringsTableToCompressedString(tableOfStrings):
    # tableLength = len(tableOfStrings)
    # spreedSheetString = ""
    # idx = 0
    # for currStr in tableOfStrings:
        # idx += 1
        # spreedSheetString += str(currStr)
        # if(idx<tableLength):
            # spreedSheetString += ";"
    # spreedSheetString += "\r\n"                                 # add CR and NL char
    # tempData = str.encode(spreedSheetString)
    # return zlib.compress(tempData) 

# def compressedStringToStringsTable(compressedString):
    # decompressed = zlib.decompress(compressedString)
    # tmpStr = str.decode(decompressed)
    # tmpTable = tmpStr.split(";")
    
    # return decompressed


    
def executeQuery(query):
    curr.execute(query)
    conn.commit()
    return query
    
def beginTransSerializable():
    query = "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE"
    curr.execute(query)  
    
def executeQueryWithoutCommit(query):
    curr.execute(query)
    return query  
 
def insertManyWithoutCommit(table, columns, dataCluster):
    tempList = []
    query = prepareInsertQuery(table, columns)
    for idx in range(len(dataCluster[0])):
        tempList.append( getDataFromCluster(dataCluster, idx) )   
    curr.executemany(query, tempList) 
    return query 

def insertOneRowWithoutCommit(table, columns, dataCluster):
    query = prepareInsertQuery(table, columns)
    curr.execute(query, dataCluster)
    return query    

def prepareInsertQuery(table, columns):
# searchTimestampFields - flag which control if function will search for timestamp fields in columns table and prepare query according to it, or not
    nbOfCol = len(columns)
    query   = "INSERT INTO "+table+" ("
    valStr  = " VALUES("
    i = 0
    for row in columns:
        query += row
        i += 1      #TIME_STAMP
        if( row.upper() in timestamp_list ): # if it is a time field                      
            valStr += "TO_DATE(:"+str(i)+",'YYYY-MM-DD HH24:MI:SS')"
        else:
            valStr += ":" + str(i)
        if i < nbOfCol:
            query += ","
            valStr += ","
    valStr += ")"        
    query += ")" + valStr  
    return query
   
def commitChanges():
    conn.commit()
 
def rollbackChanges():
    conn.rollback() 
    
def closeConnection():
    if( curr is not None ):
        curr.close()
    if( conn is not None ):
        conn.close() 

def deleteDataFromTableWhere(table, where):
    query = "DELETE FROM " + table
    if(where):
        query += " WHERE " + where
    curr.execute(query)
    conn.commit()
    return query
    
def insertOneRowIntoDB(table, columns, dataCluster):
    query = prepareInsertQuery(table, columns)
    curr.execute(query, dataCluster)
    conn.commit()
    return query

def insertManyIntoDB(table, columns, dataCluster):
    tempList = []
    query = prepareInsertQuery(table, columns)
    for idx in range(len(dataCluster[0])):
        tempList.append( getDataFromCluster(dataCluster, idx) )   
    curr.executemany(query, tempList) 
    return query 
    
    
def getManyColumnsFromDB_raw( query, nbOfColumns ):
    curr.execute(query)
    data = curr.fetchall()
    
    columns = [ [ (str(dat[i]) if (dat[i] != None) else "")  for i in range(nbOfColumns)] for dat in data ]
    return columns
    
def getDataFromCluster(dataCluster, idx):
    tempList = []
    for tempTuple in dataCluster:
        tempList.append(tempTuple[idx])
    return tempList
      
# ################### OTHERS function NOT CHECKED and NOT USED yet ##################################
    
def getOneValueFromDB(what, sourceTable):
    query = "SELECT "+ what + " FROM " + sourceTable
    curr.execute(query)
    data = curr.fetchall()
    return data[0][0]
  
def getManyColumnsFromDB_raw( query, nbOfColumns ):
    curr.execute(query)
    data = curr.fetchall()
    
    columns = [ [ (str(dat[i]) if (dat[i] != None) else "")  for i in range(nbOfColumns)] for dat in data ]
    return columns
    
def getDataFromDB_asCluster( columnsNames, sourceTable, condition ):
    nbOfColumns = len(columnsNames)
    query = prepareSelectQuery(columnsNames, sourceTable, condition)
    curr.execute(query)
    data = curr.fetchall()
    return data
 
def deleteTableData(table):
    query = "DELETE FROM " + table
    curr.execute(query)
    conn.commit()
    
def deleteDataFromTableWhere(table, where):
    query = "DELETE FROM " + table
    if(where):
        query += " WHERE " + where
    curr.execute(query)
    conn.commit()
    return query
    
# print( "hello" )
# o_query = "SELECT sample_points FROM MIC_DVC_SAMPLES WHERE sample_id='2222'"
# curr.execute(o_query)
# data = curr.fetchone()[0]
# print( data.read() )
# decompressed = zlib.decompress(data)
# print( data )
    

    