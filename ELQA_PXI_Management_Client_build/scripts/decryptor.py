from Crypto.Cipher import AES
import configparser
# AES - EAX -> Advanced Encryption Standard - encrypt-then-authenticate-then-translate

configFile  = r"C:\ELQA\config.ini"

def getLoginInfoFromFile(filename):
    config = configparser.ConfigParser()
    try:
        config.read(configFile)
        if( config['DB_SELECTOR']['db_to_use'].upper() == 'PRODUCTION'):
            conf_section = 'PRODUCTION_DB'
        else:
            conf_section = 'DEVELOPER_DB'
        temp = config[conf_section]['name'].split()
        name    = (temp[0], temp[1] )   # get db name val and nonce
        temp = config[conf_section]['user'].split()
        user    = (temp[0], temp[1] )   # get user name val and nonce
        temp = config[conf_section]['pass'].split()
        pwrd    = (temp[0], temp[1] )   # get password val and nonce
        return [name, user, pwrd, conf_section.split('_')[0]] 
        
    except KeyError as e:
        print ("[ERROR]: KeyError: " + str(e) + " - keyword not found")
        print("\tMake sure the configuration file exists in expected localization. ")
        print("\tPlease make sure that the file: '" + configFile + "' exists")
        print("\tIf NOT, You can copy it to this localization from new ELQA workspace: '..\Applications\ELQA'")
        print("\n\tIn case of further problem, please contact one of the TP4 system developer")

    return []  

def encrypt(dataToEncrypt, key):
    cipher = AES.new(key, AES.MODE_EAX)
    nonce = cipher.nonce
    ciphertext, tag = cipher.encrypt_and_digest(data)
    return (ciphertext.hex(), tag.hex(), nonce.hex())
    
def decrypt(dataToDecrypt, key, nonce):
    dataToDecrypt   = bytes.fromhex(dataToDecrypt)
    nonce           = bytes.fromhex(nonce)
    
    cipher = AES.new(key, AES.MODE_EAX, nonce=nonce)
    return cipher.decrypt(dataToDecrypt)
  
# ************* MAIN FUNCTION  *************  
    
key = b'!2Robot#$Zenek5^'

loginData = getLoginInfoFromFile(configFile)
if( len(loginData) > 0 ):
    name = decrypt( loginData[0][0], key, loginData[0][1]).decode('utf-8')
    user = decrypt( loginData[1][0], key, loginData[1][1]).decode('utf-8')
    pswd = decrypt( loginData[2][0], key, loginData[2][1]).decode('utf-8')
    dbVer = loginData[3]
else:
    print("\n[ERROR]: Problem during reading configuration file. ")
    input("\t *** Type 'ENTER' to close ***")
